import varint, socket, sys, time, json, zlib, re, pprint, colorama, termcolor
from binascii import unhexlify as uh, hexlify as he

host, port = "smp.nyedu.dev", 25575
host, port = "194.163.132.97", 8003
uname = "Alma"

colorama.init()

def loginSuccess(packet):
	id = packet[0]
	uuid_raw = he(packet[1:17]).decode()
	uuid = "-".join([uuid_raw[i*4:i*4+4] for i in range(int(len(uuid_raw)/4))])
	name = packet[17:]
	print(id, uuid, name)
	#print(f"{name.decode()}(UUID: {uuid}) logged in.")
h_login = {
	0x02: loginSuccess
}

h_play = {
    
}

def unknown(packet):
    id = varint.decode_bytes(packet)
    id_len = len(varint.encode(id))
    print(f"ID: {id}, Data: {packet[id_len:]}")

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.connect((host, port))
except ConnectionRefusedError:
    print(f"{host}:{port} is OFFLINE")
    sys.exit(1)
"""
msg = b""
msg += b"\x00"							#Packet ID: Handshake
msg += varint.encode(755)				#Protocol version: 775(Minecraft version 1.17)
#msg += varint.encode(340)				#Protocol version: 230(Minecraft version 1.12.2)
msg += varint.encode(len(host))			#Hostname length prefix
msg += host.encode()					#Hostname
msg += uh(hex(port).replace("0x", ""))	#Port
msg += varint.encode(2)					#Next state: 2=Login
msg = varint.encode(len(msg))+msg		#Packet Length Prefix
print(f"Handshake:\t{he(msg).decode()}")

s.send(msg)
msg = b"\x00"
un_len = len(uname)
msg += varint.encode(un_len)			#Username length prefix
msg += uname.encode()				    #Username
msg = varint.encode(len(msg))+msg		#Packet Length Prefix
s.send(msg)
print(f"Login start:\t{he(msg)}")

msg = s.recv(1000)
l = varint.decode_bytes(msg)
len_l = len(varint.encode(l))
packet = msg[len_l:len_l+l]
comp_tres = varint.decode_bytes(packet[1:])
print(packet)
print(f"Compression treshold: {comp_tres} bytes")
msg = msg[len_l+l:]

msg += s.recv(1000)
l = varint.decode_bytes(msg)
len_l = len(varint.encode(l))
packet = msg[len_l+1:len_l+l]
loginSuccess(packet)

msg = msg[len(packet):]
msg += s.recv(4096)
time.sleep(0.5)
while True:
    msg += s.recv(4096)

    packet_len = varint.decode_bytes(msg)
    pl_len = len(varint.encode(packet_len))
    packet = msg[pl_len:pl_len+packet_len]
    
    print(he(packet))
    msg = msg[pl_len+packet_len:]
    time.sleep(0.1)
    
#len_uncomp = varint.decode_bytes(packet)
#size = len(varint.encode(len_uncomp))
#packet = packet[0:]
try:
    print(zlib.decompress(packet))
except Exception as e:
    print(e)
    print(he(packet))




"""

def esc_color(matchobj):
    match = matchobj.group(0).replace("§", "\033[38;5;")
    #print(match[-1])
    match = match[:-1]+str(int(match[-1], 16)+24)
    match = match+"m"
    #print(match.encode())
    return match

def parse_key(dic, indent):
    for key in dic:
        if str(key) == "extra":
            parse_color(dic[key])
            return
        if isinstance(dic[key], dict):
            parse_key(dic[key], indent+2)
        elif isinstance(dic[key], str):
            s = re.sub("§.", esc_color, dic[key])
            print(" "*indent, key, s)
        elif isinstance(dic[key], list):
            for item in dic[key]:
                parse_key(item, indent+2)
        else:
            print(" "*indent, key, dic[key])
        print(colorama.Style.RESET_ALL, end="")

def parse_color(js):
    for item in js:
        curr = item["color"]
        curr = curr.replace("black", "grey")
        curr = curr.replace("dark_gray", "grey")
        curr = curr.replace("gold", "yellow")
        curr = curr.replace("dark_aqua", "blue")
        curr = curr.replace("gray", "grey")
        try:
            print(termcolor.colored(item["text"], curr ), end="")
        except Exception as e:
            print(item["color"])

msg = b""
msg += b"\x00"							#Packet ID: Handshake
#msg += varint.encode(756)				#Protocol version: 775(Minecraft version 1.17)
msg += uh('ffffffff0f')
msg += varint.encode(len(host))			#Hostname length prefix
msg += host.encode()					#Hostname
msg += uh(hex(port).replace("0x", ""))	#Port
msg += varint.encode(1)					#Next state: 1=Status
msg = varint.encode(len(msg))+msg		#Packet Length Prefix
print(f"Handshake: {he(msg).decode()}")
s.send(msg)
msg = uh("0100")	#Status request packet
print(f"Request:   {he(msg).decode()}")
s.send(msg)
timeStr= str(hex(int(time.time())).replace("0x", ""))
msg = uh("090100000000"+timeStr)	#Ping packet(with time as ping ID)
s.send(msg)
print(f"Ping:\t   {he(msg).decode()}")

time.sleep(1)

msg = s.recv(1000)
try:
    msg = msg[msg.index(b'{'):]
    msg += b"\"}"
    msg = json.loads(msg)
    msg.pop("favicon")
    #msg = msg["version"]#["name"]
    parse_key(msg, 0)
    #print(json.dumps(msg, indent=2))
except Exception as e:
    print(type(e).__name__, str(e), type(msg).__name__, "msg:", msg)




print("\n\n")

